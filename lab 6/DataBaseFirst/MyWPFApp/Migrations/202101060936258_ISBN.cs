namespace MyWPFApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ISBN : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Warriors", "MigrationVar", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Warriors", "MigrationVar");
        }
    }
}
