namespace MyWPFApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Warriors",
                c => new
                    {
                        name = c.String(nullable: false, maxLength: 128),
                        HP = c.Int(nullable: false),
                        X = c.Int(nullable: false),
                        Y = c.Int(nullable: false),
                        attack = c.Int(nullable: false),
                        defence = c.Int(nullable: false),
                        alive = c.Boolean(nullable: false),
                        type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.name);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Warriors");
        }
    }
}
