namespace MyWPFApp.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MyWPFApp.WarriorsPoolContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "MyWPFApp.WarriorsPoolContext";
        }

        protected override void Seed(MyWPFApp.WarriorsPoolContext context)
        {
        }
    }
}
