﻿using AutoMapper;
using MyWPFApp.MyDataModel;
using MyWPFApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyWPFApp.Base
{
    public class Mapping
    {
        public void Create()
        {
            Mapper.CreateMap<DataModel, DataViewModel>();
            Mapper.CreateMap<DataViewModel, DataModel>();

            Mapper.CreateMap<Warrior, WarriorViewModel>();
            Mapper.CreateMap<WarriorViewModel, Warrior>();
        }
    }
}
