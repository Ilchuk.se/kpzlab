namespace MyWPFApp
{
    using MyWPFApp.MyDataModel;
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class WarriorsPoolContext : DbContext
    {
        public WarriorsPoolContext()
            : base("name=WarriorsPoolContext")
        {
        }

        public DbSet<Warrior> Warriors { get; set; }
    }
}