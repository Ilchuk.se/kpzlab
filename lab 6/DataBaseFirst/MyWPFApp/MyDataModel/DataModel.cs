﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace MyWPFApp.MyDataModel
{
    [Serializable]
    public class DataModel
    {
        public IEnumerable<Warrior> MyWarriors { get; set; }
        public IEnumerable<Warrior> EnemyWarriors { get; set; }

        public static DataModel Load() 
        {
            using (WarriorsPoolContext context = new WarriorsPoolContext()) 
            {
                List<Warrior> InputWarriorData = context.Warriors.ToList();

                List<Warrior> GameHeroes = new List<Warrior>();
                List<Warrior> GameEnemies = new List<Warrior>();

                for (int i = 0; i < InputWarriorData.Count / 2; i++)
                {
                    GameHeroes.Add(InputWarriorData[i]);
                    GameEnemies.Add(InputWarriorData[InputWarriorData.Count - i - 1]);
                }

                DataModel GameData = new DataModel();

                GameData.MyWarriors = GameHeroes;
                GameData.EnemyWarriors = GameEnemies;

                return GameData;
            }
        }

        public void Save() 
        {
            this.WarriorListDataToDB(MyWarriors);
            this.WarriorListDataToDB(EnemyWarriors);
        }

        private void WarriorListDataToDB(IEnumerable<Warrior> warriorsData) 
        {
            foreach (Warrior w in warriorsData)
            {
                using (WarriorsPoolContext context = new WarriorsPoolContext())
                {
                    var warrior = context.Warriors.Single(sl => sl.name == w.name);
                    warrior.CopyWarriorData(w);

                    context.SaveChanges();
                }
            }

            WarriorsPoolContext context2 = new WarriorsPoolContext();
            IEnumerable<Warrior> DBWarriorData = context2.Warriors;

            foreach(Warrior w in DBWarriorData) 
            {
                if (!warriorsData.Contains(w)) ;
                //context2.Warriors.Remove(w);
            }
        }
    }
}
