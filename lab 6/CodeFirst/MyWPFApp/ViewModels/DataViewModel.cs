﻿using AutoMapper;
using MVVMTest.ViewModelc;
using MyWPFApp.MyDataModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace MyWPFApp.ViewModels
{
    public class DataViewModel : ViewModelBase
    {
        public DataViewModel()
        {
            SetAttackEnemy = new Command(ControlAttack);
            SetHealHero = new Command(ControlHeal);
        }

        private ObservableCollection<WarriorViewModel> _MyWarriors;

        public ObservableCollection<WarriorViewModel> MyWarriors
        {
            get
            {
                return _MyWarriors;
            }
            set
            {
                _MyWarriors = value;
                OnPropertyChanged("MyWarriors");
            }
        }

        private ObservableCollection<WarriorViewModel> _EnemyWarriors;

        public ObservableCollection<WarriorViewModel> EnemyWarriors
        {
            get
            {
                return _EnemyWarriors;
            }
            set
            {
                _EnemyWarriors = value;
                OnPropertyChanged("EnemyWarriors");
            }
        }

        private WarriorViewModel _SelectedHero;

        public WarriorViewModel SelectedHero
        {
            get
            {
                return _SelectedHero;
            }
            set
            {
                _SelectedHero = value;
                OnPropertyChanged("SelectedHero");
            }
        }

        private WarriorViewModel _SelectedEnemy;

        public WarriorViewModel SelectedEnemy
        {
            get
            {
                return _SelectedEnemy;
            }
            set
            {
                _SelectedEnemy = value;
                OnPropertyChanged("SelectedEnemy");
            }
        }
        public ICommand SetAttackEnemy { get; set; }
        public void ControlAttack(object args)
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            System.IO.Stream resourceStream = assembly.GetManifestResourceStream("../Sounds/swordSwipe.wav");
            SoundPlayer player = new SoundPlayer(resourceStream);
            player.PlaySync();

            Warrior hero = Mapper.Map<WarriorViewModel, Warrior>(SelectedHero);
            Warrior enemy = Mapper.Map<WarriorViewModel, Warrior>(SelectedEnemy);
            hero.AttackWarrior(enemy);
            SelectedEnemy.HP = enemy.HP;
            SelectedEnemy.alive = enemy.IsAlive();
        }

        public ICommand SetHealHero { get; set; }

        public void ControlHeal(object args)
        {
            Warrior hero = Mapper.Map<WarriorViewModel, Warrior>(SelectedHero);
            SelectedHero.HP = hero.HP + 12;
        }
    }
}
