﻿using MyWPFApp.MyDataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyWPFApp.ViewModels
{
    public class WarriorViewModel : ViewModelBase
    {
        private string _name;
        public string name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged("name");
            }
        }

        private int _HP;
        public int HP
        {
            get { return _HP; }
            set
            {
                _HP = value;
                OnPropertyChanged("HP");
            }
        }

        private int _X;
        public int X
        {
            get { return _X; }
            set
            {
                _X = value;
                OnPropertyChanged("X");
            }
        }

        private int _Y;
        public int Y
        {
            get { return _Y; }
            set
            {
                _Y = value;
                OnPropertyChanged("Y");
            }
        }

        private int _attack;
        public int attack
        {
            get { return _attack; }
            set
            {
                _attack = value;
                OnPropertyChanged("attack");
            }
        }

        private int _defence;
        public int defence
        {
            get { return _defence; }
            set
            {
                _defence = value;
                OnPropertyChanged("defence");
            }
        }

        private bool _alive;
        public bool alive
        {
            get { return _alive; }
            set
            {
                _alive = value;
                OnPropertyChanged("alive");
            }
        }

        private WarriorType _type;
        public WarriorType type 
        {
            get { return _type; }
            set
            {
                _type = value;
                OnPropertyChanged("type");
            }
        }
    }
}
