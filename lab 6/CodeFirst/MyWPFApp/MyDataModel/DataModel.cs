﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace MyWPFApp.MyDataModel
{
    [Serializable]
    public class DataModel
    {
        public IEnumerable<Warrior> MyWarriors { get; set; }
        public IEnumerable<Warrior> EnemyWarriors { get; set; }
        private static DbSet<Soldiers> InputWarriors { get; set; }
        public static DataModel Load() 
        {
            DataModel ResDM = new DataModel();
            List<Warrior> EnemiesList = new List<Warrior>();
            List<Warrior> HeroesList = new List<Warrior>();

            var KPZEntitities = new kpzEntities();
            var TempList1 = KPZEntitities.Soldiers.ToList();

            InputWarriors = KPZEntitities.Soldiers;

            foreach (Soldiers s in TempList1) 
            {
                Warrior tmpWarrior = new Warrior() { name = s.name, HP = s.HP, X = s.X, Y = s.Y, attack = s.attack, defence = s.Defence};

                if (s.type == WarriorType.Swordsman.ToString())
                    tmpWarrior.type = WarriorType.Swordsman;
                if(s.type == WarriorType.Mag.ToString())
                    tmpWarrior.type = WarriorType.Mag;
                if (s.type == WarriorType.Archer.ToString())
                    tmpWarrior.type = WarriorType.Archer;

                if (s.HP > 0)
                    tmpWarrior.alive = true;
                else 
                {
                    tmpWarrior.HP = 0;
                    tmpWarrior.alive = false;
                }

                if (s.role == "H")
                    HeroesList.Add(tmpWarrior);
                else
                    EnemiesList.Add(tmpWarrior);
            }

            ResDM.EnemyWarriors = EnemiesList;
            ResDM.MyWarriors = HeroesList;

            KPZEntitities.SaveChanges();

            return ResDM;
        }

        public void Save() 
        {
            List<Soldiers> OutputList = new List<Soldiers>();

            foreach (Warrior w in MyWarriors) 
            {
                OutputList.Add(new Soldiers() { name = w.name, HP = w.HP, X = w.X, Y = w.Y, attack = w.attack, Defence = w.defence, type = w.type.ToString(), role="H" });
            }

            foreach (Warrior w in EnemyWarriors)
            {
                OutputList.Add(new Soldiers() { name = w.name, HP = w.HP, X = w.X, Y = w.Y, attack = w.attack, Defence = w.defence, type = w.type.ToString(), role = "E" });
            }

            foreach (Soldiers s in OutputList) 
            {
                var KPZEntitities = new kpzEntities();

                foreach(Warrior w in MyWarriors) 
                {
                    using (kpzEntities context = new kpzEntities()) 
                    {
                        var warrior = context.Soldiers.SingleOrDefault(sl => sl.name == w.name);
                        if(w.name != null) 
                        {
                            warrior.HP = w.HP;
                            warrior.X = w.X;
                            warrior.Y = w.Y;

                            context.Entry(new Soldiers()).State = EntityState.Modified;
                            context.SaveChanges();
                        }
                    }
                }
            }
        }
    }
}
