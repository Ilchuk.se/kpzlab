﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace lab2Delegate
{
    class Program
    {
        static void Sword(object sender, ChangedEventArgs e)
        {
            Console.WriteLine(e.Message + string.Format(" {0} points with sword", e.Damagepoints));
        }

        static void Poison(object sender, ChangedEventArgs e)
        {
            Console.WriteLine(e.Message + string.Format(" {0} points with poison", e.Damagepoints/10));
        }

        static void Bleeding(object sender, ChangedEventArgs e)
        {
            Console.WriteLine(e.Message + string.Format(" {0} points with bleeding", e.Damagepoints / 20));
        }

        //   -------------------------------------------------
        static void HealSpell(object sender, MagicEventArgs e)
        {
            e.SpellName = "Simple healer" ;
            e.SpellType = "heal";   
            Console.WriteLine(e.Message + string.Format(" {0} , and {1} {2} points of hero", e.SpellName, e.SpellType, e.Points));
        }
        static void ExtraMagicSpell(object sender, MagicEventArgs e)
        {
            e.SpellName = "Simple magic power spell";
            e.SpellType = "increase";
            Console.WriteLine(e.Message + string.Format(" {0} , and {1} {2} points of spell effect", e.SpellName, e.SpellType, e.Points/10));
        }
        // -----------------------------------------------------
        static void Main(string[] args)
        {
            Console.WriteLine("\n Warior gets his sword, sharpens a sword with a stone and waters with poison \n");

            Warrior warrior = new Warrior();
            warrior.Changed += Sword;
            warrior.Changed += Poison;
            warrior.Changed += Bleeding;
            warrior.Magic += HealSpell;
            warrior.Magic += ExtraMagicSpell;
            warrior.MakeMove(100, 1);
            warrior.MakeMove(20, 2);

            Console.WriteLine("\n Enemy used bandage and diactivated all magic buffs \n");
            warrior.Changed -= Bleeding;
            warrior.Magic -= ExtraMagicSpell;
            warrior.MakeMove(97, 1);
            warrior.MakeMove(21, 2);

            Console.WriteLine("\n Enemy used antidote and activated antimagic \n");
            warrior.Changed -= Poison;
            warrior.Magic -= HealSpell;
            warrior.MakeMove(101, 1);

            Console.ReadKey();
        }
    }

    public class ChangedEventArgs : EventArgs
    {
        public string Message { get; private set; }
        public int Damagepoints { get; private set; }

        public ChangedEventArgs(string message, int damage)
        {
            Message = message;
            Damagepoints = damage;
        }
    }

    public class MagicEventArgs : EventArgs
    {
        public string Message { get; private set; }

        public string SpellName { get;  set; }

        public string SpellType { get;  set; }
        public int Points { get; private set; }

        public MagicEventArgs(string message, int damage)
        {
            Message = message;
            Points = damage;
        }
    }

    public delegate void ChangedFightEventHandler(object sender, ChangedEventArgs e);
    public delegate void ChangedMagicEventHandler(object sender, MagicEventArgs e);
    class Warrior 
    {
        public event ChangedFightEventHandler Changed;
        public event ChangedMagicEventHandler Magic;
        public void MakeMove(int points, int ID) 
        {
            switch (ID) 
            {
                case 1:
                    if (Changed != null)
                        Changed(this, new ChangedEventArgs(string.Format("  Make damage: "), points));
                    break;
                case 2:
                    if (Magic != null)
                        Magic(this, new MagicEventArgs(string.Format("  Read spell: "), points));
                    break;
            }
            
        } 
    }
}
