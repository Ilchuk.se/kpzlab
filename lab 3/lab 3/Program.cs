﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Fighter fr = new Fighter();
            List<Fighter> fighters = fr.GetDefaultFighters();

            WeaponPool weaponsPool = new WeaponPool();
            List<Weapon> weapons = weaponsPool.Weapons;

            Console.WriteLine("List of my fighters:");
            PrintFighters(fighters);
            Console.WriteLine("\nList of my weapons:");
            PrintWeapons(weapons);
            Console.WriteLine("\nList of fighters with weaponID 1:");
            PrintFighters(fr.FindDuplicatesByWeapon(fighters, 2));
            Console.WriteLine("\nList of weapons with damage > 32 and type - sword:");
            PrintWeapons(weaponsPool.WeaponsByTypeAndDamage(weapons,"sword", 32));

            Console.WriteLine("\nMy expansion methods: ");
            String str = "qwwerwer";
            Console.WriteLine("Is string - " + str + ". Has only text " + str.IsTextOnly());
            str = "12324234";
            Console.WriteLine("Is string - " + str + ". Has only digits " + str.IsDigitsOnly());

            Console.WriteLine("\nMy anonymus class:");
            var anonymus = new { Name = "My weapon prototype", Type="Sword", Damage = 44};
            Console.WriteLine( anonymus.GetType() + "\n Name: " + anonymus.Name + " | Type: " + anonymus.Type + " | Damage: " + anonymus.Damage);

            Console.WriteLine("\nParsing Weapons list to array and than sorting by damage:");
            Weapon [] weap = weapons.ToArray();
            Array.Sort(weap, new WeaponDamageComparer());
            weapons = weap.ToList();
            PrintWeapons(weapons);

            Console.WriteLine("\nSorting Weapon dictionary by ID: ");
            Dictionary<int, Weapon> dict = weaponsPool.MakeDictionary();
            dict = dict.OrderBy(x => x.Key).ToDictionary(x => x.Key, x => x.Value);
            foreach (KeyValuePair<int, Weapon> keyValue in dict)
            {
                Console.Write("Key: " + keyValue.Key + " - Value = ");
                Console.WriteLine(String.Format($"Weapon ID: {keyValue.Value.ID,2}, Name: {keyValue.Value.Name,20}, Damage: {keyValue.Value.Damage}"));
            }


            Console.ReadKey();
        }

        static void PrintFighters(List<Fighter> fighters) {
            foreach (Fighter f in fighters) {
                Console.WriteLine(String.Format($"Name: {f.Name, 7}, Weapon ID: {f.WeaponID, 2}, Description: {f.Description}"));
            }
        }
        static void PrintWeapons(List<Weapon> weapons)
        {
            foreach (Weapon w in weapons)
            {
                Console.WriteLine(String.Format($"Weapon ID: {w.ID,2}, Name: {w.Name,20}, Damage: {w.Damage}"));
            }
        }
    }
}
