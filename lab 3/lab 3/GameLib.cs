﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace lab_3
{
    public class Fighter
    {
        public string Name { get; set; }
        public int WeaponID { get; set; }
        public string Description { get; set; }
        public Fighter(string Name, int WeaponID, string Description)
        {
            this.Name = Name;
            this.WeaponID = WeaponID;
            this.Description = Description;
        }
        public Fighter(){}
        public List<Fighter> GetDefaultFighters()
        {
            List<Fighter> team = new List<Fighter>();

            team.Add(new Fighter("Arthur", 005, "Best swordsmen in kingdom"));
            team.Add(new Fighter("Elsa", 002, "Archer from the elf's forest"));
            team.Add(new Fighter("David", 001, "Battleship of sir Arthur"));
            team.Add(new Fighter("Bran", 004, "Robber from the tavern"));
            team.Add(new Fighter("Tran", 002, "Hired archer"));
            team.Add(new Fighter("Pak", 003, "Magician of ice"));
            team.Add(new Fighter("Sao", 003, "Magician of lava"));
            return team;
        }

        public List<Fighter> FindDuplicatesByWeapon (List<Fighter> fighters, int ID) {
            return fighters.Where(x => x.WeaponID == ID).ToList();
        }
    }

    public class Weapon
    {
        public string Name { get; set; }
        public int ID { get; set; }
        public string Type { get; set; }
        public int Damage { get; set; }
        public Weapon(string Name, int ID, int Damage, string Type)
        {
            this.Name = Name;
            this.ID = ID;
            this.Damage = Damage;
            this.Type = Type;
        }
        public Weapon() {}
    }

    public class WeaponPool
    {
        public List<Weapon> Weapons
        {
            get
            {
                return new List<Weapon>()
                {
                    new Weapon(){ Name="Silver long sword",ID=005,Damage=45,Type="sword"},
                    new Weapon(){ Name="Simple wooden bow",ID=002,Damage=13,Type="bow"},
                    new Weapon(){ Name="Simple long sword",ID=001,Damage=30,Type="sword"},
                    new Weapon(){ Name="Upgraded long sword",ID=004,Damage=37,Type="sword"},
                    new Weapon(){ Name="Simple magical wand",ID=003,Damage=20,Type="wand"},
                };
            }
        }
        public List<Weapon> WeaponsByTypeAndDamage(List<Weapon> weapons, string type, int damage)
        {
            return weapons.Where(w => w.Damage > damage && w.Type == type).Select(w => w).ToList();
        }
        public Dictionary<int, Weapon> MakeDictionary() {
            Dictionary<int, Weapon> res = new Dictionary<int, Weapon>();
            foreach (Weapon w in this.Weapons)
                res.Add(w.ID, w);
            return res;
        }
    }

    public static class MyExtensionMethods
    {
        public static bool IsDigitsOnly(this string s)
        {
            return s.All(char.IsDigit);
        }

        public static bool IsTextOnly(this string s)
        {
            return s.All(char.IsLetter);
        }
    }

    public class WeaponDamageComparer : IComparer<Weapon>
    {
        public int Compare(Weapon x, Weapon y)
        {
            if (x.Damage < y.Damage)
                return 1;
            else if (x.Damage > y.Damage)
                return -1;
            else return 0;
        }
    }
}
