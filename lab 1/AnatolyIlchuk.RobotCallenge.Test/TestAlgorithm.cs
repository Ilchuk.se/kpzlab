﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace AnatolyIlchuk.RobotCallenge.Test
{
    [TestClass]
    public class TestAlgorithm
    {
        [TestMethod]
        public void TestMoveCommand()
        {
            var algorithm = new AnatolyIlchukAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() {
                                new Robot.Common.Robot(){ Energy = 200,
                                    Position = new Position(3,5) } };
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is MoveCommand);
            Assert.AreEqual(((MoveCommand)command).NewPosition, stationPosition);
        }

        [TestMethod]
        public void TestCreateRobotCommand()
        {
            var algorithm = new AnatolyIlchukAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = new Position(3, 3), RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() {
                                new Robot.Common.Robot(){ Energy = 400,
                                    Position = new Position(1,1) } };
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is CreateNewRobotCommand);
        }

        [TestMethod]
        public void TestCollectEnergyCommand()
        {
            var algorithm = new AnatolyIlchukAlgorithm();
            var map = new Map();
            var stationPosition = new Position(2, 2);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Energy = 500, Position = new Position(4, 4), RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() {
                                new Robot.Common.Robot(){ Energy = 200,
                                    Position = new Position(2,2) } };
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is CollectEnergyCommand);
        }


        [TestMethod]
        public void TestNotCreateUselessRobotCommand()
        {
            var algorithm = new AnatolyIlchukAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = new Position(5, 5), RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() {
                                new Robot.Common.Robot(){ Energy = 200,
                                    Position = new Position(1,1) } };
            robots.Add(new Robot.Common.Robot()
            {
                Energy = 400,
                Position = new Position(1, 2)
            });

            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsFalse(command is CreateNewRobotCommand);
        }

        [TestMethod]
        public void TestCastlingAlgorithm()
        {
            var algorithm = new AnatolyIlchukAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            var stationPosition2 = new Position(3, 3);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition2, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() {
                                new Robot.Common.Robot(){ Energy = 900,
                                    Position = new Position(1,1) } };
            robots.Add(new Robot.Common.Robot()
            {
                Energy = 400,
                Position = new Position(1, 2)
            });

            var command1 = algorithm.DoStep(robots, 0, map);
            var command2 = algorithm.DoStep(robots, 1, map);

            Assert.IsTrue(command1 is MoveCommand);
            Assert.AreEqual(((MoveCommand)command1).NewPosition, stationPosition2);

            Assert.IsTrue(command2 is MoveCommand);
            Assert.AreEqual(robots[0].Position, stationPosition);
        }

        [TestMethod]
        public void TestFightMethod()
        {
            var algorithm = new AnatolyIlchukAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() {
                                new Robot.Common.Robot(){ Energy = 200,
                                    Position = new Position(1,1) } };
            robots.Add(new Robot.Common.Robot()
            {
                Energy = 900,
                Position = new Position(3, 3)
            });

            var command = algorithm.DoStep(robots, 1, map);

            Assert.IsTrue(command is MoveCommand);
            Assert.AreEqual(((MoveCommand)command).NewPosition, stationPosition);
        }

        [TestMethod]
        public void TestNotFightMethod()
        {
            var algorithm = new AnatolyIlchukAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() {
                                new Robot.Common.Robot(){ Energy = 300,
                                    Position = new Position(1,1) } };
            robots.Add(new Robot.Common.Robot()
            {
                Energy = 100,
                Position = new Position(3, 3)
            });

            var command = algorithm.DoStep(robots, 1, map);

            Assert.IsFalse(command is MoveCommand);
        }

        [TestMethod]
        public void TestNotMoveCommand()
        {
            var algorithm = new AnatolyIlchukAlgorithm();
            var map = new Map();
            var stationPosition = new Position(40, 40);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() {
                                new Robot.Common.Robot(){ Energy = 100,
                                    Position = new Position(3,5) } };
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsFalse(command is MoveCommand);
        }

        [TestMethod]
        public void TestFightIfStationBusyCommand()
        {
            var algorithm = new AnatolyIlchukAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            var stationPosition2 = new Position(5, 5);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition2, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() {
                                new Robot.Common.Robot(){ Energy = 200,
                                    Position = stationPosition } };
            robots.Add(new Robot.Common.Robot()
            {
                Energy = 900,
                Position = stationPosition2
            });
            robots.Add(new Robot.Common.Robot()
            {
                Energy = 900,
                Position = new Position(3, 3)
            });

            var command = algorithm.DoStep(robots, 2, map);

            Assert.IsTrue(command is MoveCommand);
            Assert.AreEqual(((MoveCommand)command).NewPosition, stationPosition);
        }
    }
}