﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyWPFApp.MyDataModel
{
    [Serializable]
    public enum WarriorType {Swordsman, Archer, Mag };
    [Serializable]
    public class Warrior
    {
        public string name { get; set; }
        public int HP { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int attack { get; set; }
        public int defence { get; set; }
        public bool alive { get; set; }
        public WarriorType type { get; set; }

        public Warrior(string name, int HP, int X, int Y, int attack, int defence, WarriorType type)
        {
            this.name = name;
            this.HP = HP;
            this.X = X;
            this.Y = Y;
            this.attack = attack;
            this.defence = defence;
            this.type = type;
            if(this.HP > 0)
                this.alive = true;
            else
                this.alive = false;
        }

        public void AttackWarrior (Warrior enemy)
        {
            if (this.attack / enemy.defence > 0.5)
                enemy.HP -= (int)(this.attack * ((double)this.attack / (double)enemy.defence));
            else
                enemy.HP -= (int)(this.attack * 0.5);

            enemy.IsAlive();
        }

        public bool IsAlive() 
        {
            if (this.HP <= 0)
            {
                alive = false;
                HP = 0;
                return false;
            }
            else return true;
        }
    }
}
