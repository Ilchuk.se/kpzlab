﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyWPFApp.MyDataModel
{
    [Serializable]
    public class DataModel
    {
        public IEnumerable<Warrior> MyWarriors { get; set; }
        public IEnumerable<Warrior> EnemyWarriors { get; set; }

        public static string DataPath = "gameFiles.dat";

        public DataModel() 
        {
            MyWarriors = new List<Warrior>()
            {
                new Warrior("Derek", 100, 0, 0, 15, 20, WarriorType.Swordsman),
                new Warrior("Alfred", 70, 0, 1, 20, 15,  WarriorType.Archer),
                new Warrior("Mike", 80, 1, 0, 18, 17, WarriorType.Mag),
            };

            EnemyWarriors = new List<Warrior>()
            {
                new Warrior("Darius", 90, 3, 3, 12, 15, WarriorType.Mag),
                new Warrior("Argus", 70, 3, 4, 15, 17, WarriorType.Swordsman),
                new Warrior("Alucard", 100, 4, 3, 20, 10, WarriorType.Archer),
            };
        }

        public static DataModel Load() 
        {
            if (File.Exists(DataPath)) 
            {
                return DataSerializer.DeserializeItem(DataPath);
            }
            return new DataModel();
        }

        public void Save() 
        {
            DataSerializer.SarializeData(DataPath, this);
        }
    }
}
