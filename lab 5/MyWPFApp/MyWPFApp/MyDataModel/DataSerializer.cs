﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace MyWPFApp.MyDataModel
{
    public class DataSerializer
    {
        public static void SarializeData(string fileName, DataModel data) 
        {
            var formatter = new BinaryFormatter();
            var s = new FileStream(fileName, FileMode.Create);
            formatter.Serialize(s, data);
            s.Close();
        }

        public static DataModel DeserializeItem(string fileName) 
        {
            var s = new FileStream(fileName, FileMode.Open);
            var formatter = new BinaryFormatter();
            return (DataModel)formatter.Deserialize(s);
        }
    }
}
