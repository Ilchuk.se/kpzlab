﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAPI_CURD.Models;

namespace WebAPI_CURD.Base
{
    public static class Mapping
    {
        public static void Create()
        {
            Mapper.CreateMap<Soldier, WarriorViewModel>();
            Mapper.CreateMap<WarriorViewModel, Soldier>();
        }
    }
}