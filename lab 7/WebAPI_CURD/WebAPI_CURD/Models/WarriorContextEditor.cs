﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using WebAPI_CURD.Base;

namespace WebAPI_CURD.Models
{
    public class WarriorContextEditor
    {
        static WarriorContextEditor() 
        {
            Mapping.Create();
        }

        private static WarriorEntities WarriorDBContext = new WarriorEntities();
        
        public static List<Soldier> Get() 
        {
            List<Soldier> DBData = WarriorDBContext.Soldiers.ToList();
            return DBData;
        }
        
        public static List<WarriorViewModel> GetWarriorViewModels() 
        {
            List<Soldier> data = WarriorContextEditor.Get();

            if (data.Count() == 0)
                return null;

            return data.Select(c => Mapper.Map<Soldier, WarriorViewModel>(c)).ToList<WarriorViewModel>();
        }
        
        public static void InsertWarrior(WarriorViewModel NewWarrior) 
        {
            Soldier WarriorToInsert = Mapper.Map<WarriorViewModel, Soldier>(NewWarrior);

            WarriorDBContext.Soldiers.Add(WarriorToInsert);
            WarriorDBContext.SaveChanges();
        }
        
        public static void DeleteWarriorById(int ID) 
        {
            var warrior = WarriorDBContext.Soldiers
                        .Where(w => w.SoldierID == ID)
                        .FirstOrDefault();

            WarriorDBContext.Entry(warrior).State = System.Data.Entity.EntityState.Deleted;
            WarriorDBContext.SaveChanges();
        }
        
        public static bool UpdateWarrior(WarriorViewModel warrior) 
        {
            var checkExistingWarrior = WarriorDBContext.Soldiers.Where(w => w.name == warrior.name)
                                                                .FirstOrDefault<Soldier>();

            if (checkExistingWarrior != null)
            {
                checkExistingWarrior.name = warrior.name;
                checkExistingWarrior.X = warrior.X;
                checkExistingWarrior.Y = warrior.Y;
                checkExistingWarrior.attack = warrior.attack;
                checkExistingWarrior.Defence = warrior.Defence;
                checkExistingWarrior.HP = warrior.HP;
                checkExistingWarrior.type = warrior.type;
                checkExistingWarrior.role = warrior.role;
                checkExistingWarrior.SoldierID = warrior.SoldierID;

                WarriorDBContext.SaveChanges();
                return true;
            }
            else
                return false;
        }
    }
}