﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI_CURD.Models
{
    public class WarriorViewModel
    {
        public int SoldierID { get; set; }
        public string name { get; set; }
        public int HP { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int attack { get; set; }
        public int Defence { get; set; }
        public string type { get; set; }
        public string role { get; set; }
    }
}