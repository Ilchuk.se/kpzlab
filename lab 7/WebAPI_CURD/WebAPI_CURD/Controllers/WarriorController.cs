﻿using AutoMapper;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI_CURD.Base;
using WebAPI_CURD.Models;

namespace WebAPI_CURD.Controllers
{
    public class WarriorController : ApiController
    {
        //GET
        public IHttpActionResult Get() 
        {
            IList<WarriorViewModel> warrior = WarriorContextEditor.GetWarriorViewModels();

            if (warrior.Count == 0 || warrior == null)
                return NotFound();

            return Ok(warrior);
        }

        //POST
        public IHttpActionResult Post(WarriorViewModel warrior) 
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid data. Please recheck.");

            WarriorContextEditor.InsertWarrior(warrior);

            return Ok();
        }

        //PUT
        public IHttpActionResult Put(WarriorViewModel warrior) 
        {
            if (!ModelState.IsValid)
                return BadRequest("This is invalid model. Please recheck!");

            if(WarriorContextEditor.UpdateWarrior(warrior))
                return Ok();
            else
                return NotFound();       
        }

        //DELETE
        public IHttpActionResult Delete(int id) 
        {
            if (id <= 0)
                return BadRequest("Please enter valid Customer ID");

            WarriorContextEditor.DeleteWarriorById(id);

            return Ok();
        }
    }
}
