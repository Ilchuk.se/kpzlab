﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace lab_4
{
    interface IPersonModel {
        int HP { get; set; }
        string Name { get; set; }
        int Speed { get; set; }
        int Energy { get; set; }
        int X { get; set; }
        int Y { get; set; }
    };

    interface IWariorModel {
        int Damage { get; set; }
        int Deffence { get; set; }
        int AttackEnergy { get; set; }
    };

    public class Swordsman : Soldier
    {
        public override void MakeDamage(Soldier sl, double coef)
        {
            this.CalculateEnergyAttack((int)(this.Damage * coef));
            if (Math.Abs(sl.X - this.X) <= 1 && Math.Abs(sl.Y - this.Y) <= 1 && this.Damage > sl.Deffence)
                sl.MakeDeffence(this, (int)(this.Damage * coef));
        }
        public override void MakeDeffence(Soldier sl, int Damage)
        {
            this.CalculateDeffenceHP(Damage);
        }
        public static void SortFighterByHP(ref List<Swordsman> SwList) 
        {
            SwList.OrderBy(x => x.HP);
        }

        public static void GetFightersWithHighestHP(List<Swordsman> SwList, int num, out int status) 
        {
            if (SwList.Count() == 0 || SwList.Count() < num) status = 0;
            else status = 1;
            Swordsman.SortFighterByHP(ref SwList);
            SwList.RemoveRange(num - 1, SwList.Count() - num);
        }
    }

    public abstract class Soldier : IPersonModel, IWariorModel
    {
        public int HP { get; set; }
        public string Name { get; set; }
        public int Speed { get; set; }
        public int Energy { get; set; }
        public int Damage { get; set; }
        public int AttackEnergy { get; set; }
        public int Deffence { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        private string Meta { get; set; }

        protected void CalculateEnergyAttack(int damagePoints) {
            double coef = ((double)damagePoints / (double)this.Damage);
            double dEnergyAttack = (double)this.AttackEnergy;
            double dEnergy = (double)this.Energy;

            this.Energy = (int)(dEnergy - dEnergyAttack);
            if(coef > 1)
                this.Energy = (int)(dEnergy - dEnergyAttack - dEnergy * coef / 10);
        }

        protected void CalculateDeffenceHP(int damagePoints) {
            double coef = ((double)damagePoints / (double)this.Deffence);

            if (coef >= 1)
                this.HP = this.HP - damagePoints;
            else
                this.HP = this.HP - (int)((double)(damagePoints * coef) );
        }

        public abstract void MakeDamage(Soldier sl, double coef);
        public abstract void MakeDeffence(Soldier sl, int Damage);

        
    };

    public class GameMap 
    {
        static GameMap() {
            Console.WriteLine("\nMy static construcor\n");
            num = 1;
            //X = 5;
        }

        static int num;
        private int X { get; set; }
        private int Y { get; set; }
        public MapType mapType { get; set; }
        public List<Swordsman> SwList { get; set; }
        public GameMap(int X, int Y) {
            this.X = X;
            GameMap.num = 5;
            this.Y = Y;
        }

        public GameMap(int X, int Y, MapType mapType) : this(X, Y) {
            this.mapType = mapType;
        }
    };

    public class OverideGameMap : GameMap
    {
        protected List<Swordsman> ProtectedSwList { get; set; }
        private List<Swordsman> PrivateSwList { get; set; }

        public List<Swordsman> GetProtectedSwList()
        {
            return this.ProtectedSwList;
        }
        public List<Swordsman> GetPrivateSwList()
        {
            return this.PrivateSwList;
        }
        public void SetProtectedSwList(List<Swordsman> SwList)
        {
            this.ProtectedSwList = SwList;
        }
        public void SetPrivateSwList(List<Swordsman> SwList)
        {
            this.PrivateSwList = SwList;
        }
        public OverideGameMap(int x, int y) : base(x, y) { }
        public OverideGameMap(int x, int y, MapType mapType) : base(x, y, mapType) { }

        public static implicit operator OverideGameMap(int X1) 
        {
            OverideGameMap MyMap = new OverideGameMap(X1, X1);
            return MyMap;
        }

        public static explicit operator OverideGameMap(int[] X)
        {
            OverideGameMap MyMap = new OverideGameMap(X[0], X[1]);
            return MyMap;
        }
    };

    public class SonMap : OverideGameMap { 
        public SonMap(int x, int y) : base(x, y) { }

        public void Foo() 
        {
          //this.
        }
    };

    public enum MapType { Easy, Normal, Hard};
    public enum MapWeatherType { WithSand, WithRain, WithWind };
}
