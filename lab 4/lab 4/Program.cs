﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace lab_4
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Swordsman sw1 = new Swordsman(){ AttackEnergy=10, Damage=15, Energy=200, HP=300, Name="Derek", Deffence=11, Speed=7, X=1, Y=1 };
              Swordsman sw2 = new Swordsman() { AttackEnergy = 7, Damage = 12, Energy = 300, HP = 250, Name = "Solomon", Deffence = 10, Speed = 10, X = 1, Y = 2 };
            Swordsman sw3 = new Swordsman() { AttackEnergy = 11, Damage = 16, Energy = 180, HP = 220, Name = "Peter", Deffence = 12, Speed = 8, X = 2, Y = 1 };
            PrintSwordsman(sw1);
              Console.WriteLine("-------------------");
              PrintSwordsman(sw2);
              sw1.MakeDamage(sw2, 1.5);
              Console.WriteLine("-------------------");
              PrintSwordsman(sw2);
              Console.WriteLine("-------------------");
              PrintSwordsman(sw1);
            //------------------------------------------------------
            int size = 30;
            int[] sz = { 10, 20 }; 

            //Expicit and impilicit
            OverideGameMap GMap = size;
            OverideGameMap GMap2 = (OverideGameMap)sz;

            Console.WriteLine("Logical Operations with ENUM:");
            GMap.mapType = MapType.Easy ^ MapType.Hard;
            Console.WriteLine(GMap.mapType);

            GMap.mapType = MapType.Easy | MapType.Hard;
            Console.WriteLine(GMap.mapType);

            GMap.mapType = MapType.Easy & MapType.Hard;
            Console.WriteLine(GMap.mapType);

            //-----------------------------------------------
            Console.WriteLine("\n Using ref\n");
            List<Swordsman> SwList = new List<Swordsman>();
            SwList.Add(sw1);
            SwList.Add(sw2);
            SwList.Add(sw3);

            Swordsman.SortFighterByHP(ref SwList);
            foreach (Swordsman s in SwList) 
            {
                Console.WriteLine("------------------------");
                PrintSwordsman(s);
            }

            Console.WriteLine("\n Using out\n");
            Swordsman.GetFightersWithHighestHP(SwList, 2, out int result);
            foreach (Swordsman s in SwList)
            {
                Console.WriteLine("------------------------");
                PrintSwordsman(s);
            }
            Console.WriteLine( "\nStatus: "+ result);
            //-----------------------------------------------
            Console.WriteLine("\n Using boxing and unboxing\n");

            Object boxedSwordsman = sw1;
            Swordsman unboxedSwordsman = (Swordsman)boxedSwordsman;
            Console.WriteLine("Type of boxed swordsman: " + boxedSwordsman + "\n\nUnboxed swordsman:");

            PrintSwordsman(unboxedSwordsman);
            //--------------------------------------------
            */

            /*
            var sw = new Stopwatch();
            sw.Start();

            Swordsman sw5 = new Swordsman() { AttackEnergy = 10, Damage = 15, Energy = 200, HP = 300, Name = "Derek", Deffence = 11, Speed = 7, X = 1, Y = 1 };
            Object boxedSwordsman = sw5;
            Swordsman unboxedSwordsman; 
            for (int i = 0; i < 100000000; i++)
                unboxedSwordsman = (Swordsman)boxedSwordsman;

            sw.Stop();
            Console.WriteLine($"Time Spent: {sw.ElapsedMilliseconds}");
            //--------------------------------------
            GameMap GM = new GameMap(10,20);
            Object boxedMap;
            sw.Start();
            for (int i = 0; i < 100000000; i++)
                boxedMap = GM;
            sw.Stop();
            Console.WriteLine($"Time Spent: {sw.ElapsedMilliseconds}");*/

            Console.ReadKey();
        }

        public static void PrintSwordsman(Swordsman sw) {
            Console.WriteLine($"Name: {sw.Name, 8}\nHP: {sw.HP, 4}\nDamage: {sw.Damage,3}\nEnergy: {sw.Energy, 3}\nDeffence: {sw.Deffence, 3}");
        }
    }
}
